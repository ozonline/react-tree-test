const HTMLWebpackPlugin = require('html-webpack-plugin');
const htmlWebpackPluginConfig = new HTMLWebpackPlugin({
    template: __dirname + '/src/app/index.html',
    filename: 'index.html',
    inject: 'body'
});

module.exports = {
    entry: __dirname + '/src/app/index.js',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test:/\.css$/,
                use:['style-loader','css-loader']
            }
        ]
    },
    output: {
        filename: 'index.js',
        path: __dirname + '/src/build'
    },
    plugins: [htmlWebpackPluginConfig],
    stats: { children: false }
};