import React, { Component }             from 'react'
import { Modal as ReactBootstrapModal } from 'react-bootstrap'

class ModalBody extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { children } = this.props

    return (
      <div>
        <ReactBootstrapModal.Body>
          {children}
        </ReactBootstrapModal.Body>
      </div>
    )
  }
}

export default ModalBody
