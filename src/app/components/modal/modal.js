import React from 'react'
import { Modal as ReactBootstrapModal } from 'react-bootstrap'

class Modal extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false
        }
    }

    static getDerivedStateFromProps(props) {
        const { modalKey, modalState } = props
        if (modalKey === modalState.modalKey) {
            return { show: modalState.modalIsOpen }
        }
        return { show: false }
    }

    render() {
        const { show } = this.state
        const { children, closeAction, modalIsOpen, modalKey } = this.props
    
        return (
            <ReactBootstrapModal
            onHide={() => this.handleClose({modalKey, closeAction})}
            show={show}>
                {children}
            </ReactBootstrapModal>
        )
    }

    handleClose(props) {
        const { closeAction, modalKey } = props
        closeAction(modalKey)
    }

}

export default Modal
  