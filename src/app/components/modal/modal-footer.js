import React, { Component } from 'react'
import { Modal as ReactBootstrapModal } from 'react-bootstrap'

class ModalFooter extends Component {
    constructor(props) {
        super(props)
    }

    getContent() {
        const { node, children, closeAction, deleteAction } = this.props

        return (
            <div>
                <div onClick={closeAction} className="cancel-btn btn btn-link">Cancel</div>
                <button type="button"
                    onClick={() => { deleteAction(node.id, closeAction) }}
                    className="btn btn-danger">
                    Delete
                </button>
                <div className="custom-modal-footer-actions">{children}</div>
            </div >
        )
    }

    render() {
        const content = this.getContent()

        return (
            <div>
                <ReactBootstrapModal.Footer>
                    {content}
                </ReactBootstrapModal.Footer>
            </div>
        )
    }
}

export default ModalFooter
