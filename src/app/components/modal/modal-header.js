import React, { Component }             from 'react'
import { Modal as ReactBootstrapModal } from 'react-bootstrap'

class ModalHeader extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { children, title } = this.props

    return (
    <div className=''>
        <ReactBootstrapModal.Header>
          <ReactBootstrapModal.Title>
            <span className="h3">{title}</span>
          </ReactBootstrapModal.Title>
          {children}
        </ReactBootstrapModal.Header>
      </div>
    )
  }
}

export default ModalHeader
