import React from 'react'

class EditableArea extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            content: this.props.content || 'EditableArea',
            editing: true,
        }
        this.originalContent = this.props.content || 'EditableArea'
    }

    handleKeyPress(e) {
        if (e.key === 'Enter') {
            e.preventDefault()
            e.target.blur()
            this.save()
        }
    }

    setEditing() {
        this.setState({editing: true})
    }

    save() {
        let newContent = this.area.textContent
        if (newContent != '' && newContent != this.originalContent) {
            const { callbackParent } = this.props
            this.setState({content: this.area.textContent})
            callbackParent(this.area.textContent)
        } else {
            this.area.textContent = this.originalContent
        }
    }


    render() {
        return (
            <div ref={e => this.area = e} 
            contentEditable={this.state.editing} 
            onKeyPress={this.handleKeyPress.bind(this)} 
            onBlur={this.save.bind(this)} 
            onClick={this.setEditing.bind(this)}
            onFocus={this.setEditing.bind(this)}
            suppressContentEditableWarning={true}>
                {this.state.content}
            </div>
        )
    }
}

export default EditableArea