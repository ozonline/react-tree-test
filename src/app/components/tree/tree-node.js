import React from 'react'
import { FaExternalLinkAlt, FaMinus, FaPlus, FaReact } from 'react-icons/fa'
import styled from 'styled-components'
import EditableArea from '../editable-area/editable-area';

const getPaddingLeft = (level) => {
    let paddingLeft = parseInt(level) * 24
    return paddingLeft
}

const StyledTreeNode = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    padding: 4px 0;
    padding-left: ${props => getPaddingLeft(props.level)}px;
`

const NodeIcon = styled.div`
    font-size: 24px;
    margin: 4px;
    line-height: 1px;
`

const NodeLabel = styled.span`
    font-size: 24px;
`

const ActionIcon = styled.span`
    margin: 4px;
    cursor: pointer;
    line-height: 1px;
`

const LinkIcon = styled.a`
    color: black !important;
    outline: none;

    &:hover {
        color: black
    }

    &:visited {
        color: black
    }
`

const DeleteIcon = (props) => {
    if (props.node.id == props.rootId && !props.node.isRoot) {
        return (
            <ActionIcon>
                <FaMinus onClick={() => {props.openModal(props.rootId)}} />
            </ActionIcon>
        )
    }
    return (
        <ActionIcon />
    )
}

let TreeNode = (props) => {
    const { node, 
        getNodes,
        addNode,
        updateNodeName,
        openModal, 
        level,
        rootId
    } = props

    const href = `./${node.id}`

    return (
        <React.Fragment>
            <StyledTreeNode level={level}>
                <NodeIcon>
                    <FaReact />
                </NodeIcon>
                <NodeLabel>
                    <EditableArea 
                        content={node.name}
                        callbackParent={(content) => {updateNodeName(node.id, content)}}
                    />
                </NodeLabel>
                <ActionIcon>
                    <FaPlus onClick={() => (addNode(node.id))} />
                </ActionIcon>
                <ActionIcon>
                    <LinkIcon target="_blank" href={href}>
                        <FaExternalLinkAlt />
                    </LinkIcon>
                </ActionIcon>
                <DeleteIcon 
                    node={node}
                    rootId={rootId} 
                    openModal={openModal} 
                />
            </StyledTreeNode>
            { getNodes(node).map((node) => (
                    <TreeNode 
                        {...props}
                        key={node.id}
                        node={node}
                        level={parseInt(level) + 1}
                    />
            ))}
        </React.Fragment>
    )
};

export default TreeNode;
