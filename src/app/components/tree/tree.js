import React from 'react'
import TreeNode from './tree-node'
import {
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter
} from '../modal'

const apiUrl = 'http://127.0.0.1:3000/api/tree'

class Tree extends React.Component {

    constructor(props) {
        super(props)
        this.state = {modalState: {
            modalKey: null,
            modalIsOpen: false
        }}
        this.rootId = this.props.match.params.id || ''
        
        this.getNodes = this.getNodes.bind(this)
        this.addNode = this.addNode.bind(this)
        this.updateNodeName = this.updateNodeName.bind(this)
        this.openModal = this.openModal.bind(this)
        this.closeModal = this.closeModal.bind(this)
    }

    componentDidMount() {
        this.getTree()
    }

    getRootNodes() {
        const { nodes } = this.state
        for (var key in nodes) {
            if (nodes[key].isRoot || key == this.rootId) {
                return [nodes[key]]
            }
        }
        return []
    };

    getNodes(node) {
        const { nodes } = this.state
        if (!node.children || node.children.length < 1) return []
        return node.children.map(name => nodes[name])
    }

    getTree() {
        fetch(`${apiUrl}/${this.rootId}`)
            .then(response => response.json())
            .then(data => this.setState({ nodes: data.response.data }))
            .catch(error => {
                alert(`An error occurred while attempting to get the tree!\n\n`, error);
            })
    }

    addNode(parentId) {
        fetch(`${apiUrl}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: 'Node',
                parent: parentId
            })
        })
        .then(response => response.json())
        .then(data => {
            let newNode = data.response.data
            let newState = Object.assign({}, this.state)
            newState.nodes[newNode.id] = newNode
            newState.nodes[newNode.parent].children.unshift(newNode.id)
            this.setState(newState)
        })
        .catch(error => {
            alert(`An error occurred while attempting to add the child node!\n\n`, error);
        })
    }

    updateNodeName(id, newName) {
        fetch(`${apiUrl}/${id}/name`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: newName
            })
        })
        .then(response => response.json())
        .then(data => {
            const { id, name } = data.response.data
            let newState = Object.assign({}, this.state)
            newState.nodes[id].name = name
            this.setState(newState)
        })
        .catch(error => {
            alert(`An error occurred while attempting update the node's name!\n\n`, error);
        })
    }

    deleteNode(id, callback) {
        console.log('deleting', id)
        fetch(`${apiUrl}/${id}`, {
            method: 'DELETE'
        })
        .then(response => response.json())
        .then(data => {
            callback(data.response.data)
            window.location.href='/'
        })
        .catch(error => {
            callback(id)
            alert(`An error occurred while attempting to delete the node!\n\n`, error);
        })
    }

    openModal(modalKey) {
        this.setState({modalState: {
            modalKey: modalKey,
            modalIsOpen: true
        }})
    };

    closeModal(modalKey) {
        this.setState({modalState: {
            modalKey: modalKey,
            modalIsOpen: false
        }})
    }

    render() {
        const rootNodes = this.getRootNodes()
        const nodes = rootNodes.map((node) => 
            <div key={node.id}>
                <TreeNode 
                    key={node.id}
                    node={node}
                    rootId={this.rootId}
                    getNodes={this.getNodes}
                    addNode={this.addNode}
                    updateNodeName={this.updateNodeName}
                    redirectToSubTree={this.redirectToSubTree}
                    openModal={this.openModal}
                    closeModal={this.closeModal}
                    level="0"
                />
                <Modal
                    modalKey={node.id}
                    modalState={this.state.modalState}
                    closeAction={this.closeModal}
                    deleteNode={this.deleteNode}
                >
                    <ModalHeader title="DELETE NODE" />
                    <ModalBody>
                        <p className="copy">
                            Are you sure you want to delete this node and all its decendent nodes?<br />
                            <br />
                            Name: {node.name}<br />
                            ID: {node.id}<br />
                            <br />
                            <span className="text-danger">This action is irreversible!</span>
                        </p>
                    </ModalBody>
                    <ModalFooter node={node} closeAction={this.closeModal} deleteAction={this.deleteNode} />
                </Modal>
            </div>  
        )

        return (
            <div>
                {nodes}
            </div>
        )
    }

}

export default Tree
