import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Home from '../containers/home/home-page'

class Routes extends React.Component {
    render() {
        return (
            <Router>
                <div>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/:id" component={Home} />
                </div>
            </Router>
        )
    }
}

export default Routes