import React from 'react'
import Tree from '../../components/tree/tree'

class Home extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="homepage">
                <Tree {...this.props} />
            </div>
        )
    }
}

export default Home