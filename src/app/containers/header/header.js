import React from 'react'

const Header = () => {
    return (
        <header className="page-header">
            <h1 className="h1 text-center">React Tree Test</h1>
        </header>
    )
}

export default Header