import React from 'react'
import Routes from '../../routes/routes'
import Header from '../header/header'

class App extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <main className="container">
                    <Routes />
                </main>
            </div>
        );
    }
}

export default App