import React from 'react'
import ReactDom from 'react-dom'
import App from './containers/app/app'

import './styles.css'

ReactDom.render(
    <App />,
    document.getElementById('app')
)