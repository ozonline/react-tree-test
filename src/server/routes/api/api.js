const express = require('express');
const router = express.Router();

// /* API test. */
// router.get('/', (req, res) => {
//   res.send('API is working');
// });

router.use('/tree', require('./tree'));

module.exports = router;