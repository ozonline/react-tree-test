const express = require('express');
const router = express.Router();
const uuidv4 = require('uuid/v4');

const data = require('../../mock-db/data.json');

router.delete('/:id', (req, res) => {
    const id = req.params.id;
    if (!data[id]) resourceNotFound(res, id);

    recursiveDelete(id);
    sendResponse(res, id);
});

router.get('/', (req, res) => {
    sendResponse(res, data);
});

router.get('/:id', (req, res) => {
    const id = req.params.id;
    const subTree = buildSubTree(id);

    let subTreeObj = {};

    subTree.forEach(node => {
        subTreeObj[node.id] = node;
    })

    sendResponse(res, subTreeObj);
})

router.get('/node/:id', (req, res) => {
    const id = req.params.id;
    if (!data[id]) resourceNotFound(res, id);
    sendResponse(res, data[id]);
})

router.post('/', (req, res) => {
    // Create the new node object
    const newNode = {
        id: uuidv4(),
        name: req.body.name,
        parent: req.body.parent,
        children: []
    }

    // Add the new node to the mock database
    data[newNode.id] = newNode;
    data[req.body.parent].children.unshift(newNode.id);

    sendResponse(res, newNode);
});

router.put('/:id/name', (req, res) => {
    const id = req.params.id;
    if (!data[id]) resourceNotFound(res, id);

    // Update the node's name
    if (req.body.name) data[id].name = req.body.name;

    sendResponse(res, data[id]);
});

// Helper functions to keep everything DRY
let resourceNotFound = (res, id) => {
    res.status(404);
    res.json({
        response: {
            success: 0,
            message: `Resource with id ${id} was not found.`
        }
    });
    res.end;
}

let sendResponse = (res, data) => {
    res.status(200);
    res.json({
        response: {
            success: 1,
            data: data
        }
    });
}

let recursiveDelete = (id) => {
    // Delete child reference from the parent node
    if(data[id].parent) {
        const parent = data[id].parent;
        data[parent].children.splice(data[parent].children.indexOf(id), 1);
    }

    // Recursively delete all children
    const children = data[id].children.map(node => node);
    children.forEach(node => {
        recursiveDelete(node);
    });

    // Delete the node
    delete data[id];
}

let buildSubTree = (id, tree) => {
    let subTree = tree || [];

    // Recursively get all children
    const children = data[id].children.map(node => node);
    children.forEach(node => {
        buildSubTree(node, subTree);
    });

    subTree.unshift(data[id]);

    return subTree;
}

module.exports = router;
