const bodyParser = require('body-parser');
const express = require('express');
const http = require('http');
const path = require('path');

// Express
var app = express();

// Settings
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// CORS
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "POST, GET");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// Static path to dist
app.use(express.static(path.join(__dirname, '../build')));

// Routes
app.use('/api', require('./routes/api/api'));

// All other routes return the index file
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../build/index.html'));
});

// Web and http services
app = http.createServer(app);
const PORT = process.env.PORT || 3000;

// Start server
app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
});