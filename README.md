# React Tree Test

React Tree Test is an application developed according to the following assignment.

## Assignment

* Build a "rooted" evolutionary tree using React and a backend language/framework/database of your choosing.
* You can assume that the root of this tree is AMOEBA. This isn't a Biology test.
* On startup, we should see this common ancestor and have the ability to add an arbitrary number of evolutionary branches off of that "node."
* As nodes are added to this evolutionary tree, we should be able to continually add both sibling and child branches at will.
* You should be able to edit the name of any visible node (child/ancestor/sibling/etc) in place. This update should persist across page reloads and redirects.
* There should be an option to click on any node in this tree and redirect to a new page on which that "clicked" node is now the root. This page should have a unique URL.
* There should be an option to delete a given node from its unique page. There should be a warning and subsequently delete the entire subtree as well. The only un-deletable node is "The Amoeba" since it's our root node.
* You should provide a REST API to handle updates to this evolutionary tree and requests for relevant subtrees.
* Provide a github URL with readable commit history

NOTE: Please focus most of your attention on the frontend. Give thought to layout and presentation. If time is an issue, we'd prefer a stubbed backend to a partial frontend.

## Build

Run `npm run dev` to build the project in develpoment mode. The build artifacts will be stored in the `build/` directory. Run `npm run build` for a production build.

## Launch

Run `npm run start` to launch the application with the node server and web API.
